package pl.przemek;

import javafx.scene.control.ListView;

import java.time.LocalDate;

public class LocalEvent {

    private LocalDate myDate;
    private ListView myListView;
    private String myTheme;
    private String myDescription;

    public LocalEvent(LocalDate myDate, String myTheme, String myDescription) {
        this.myDate = myDate;
        this.myTheme = myTheme;
        this.myDescription = myDescription;
    }

    public LocalDate getMyDate() {
        return myDate;
    }

    public void setMyDate(LocalDate myDate) {
        this.myDate = myDate;
    }

    public ListView getMyListView() {
        return myListView;
    }

    public void setMyListView(ListView myListView) {
        this.myListView = myListView;
    }

    public String getMyTheme() {
        return myTheme;
    }

    public void setMyTheme(String myTheme) {
        this.myTheme = myTheme;
    }

    public String getMyDescription() {
        return myDescription;
    }

    public void setMyDescription(String myDescription) {
        this.myDescription = myDescription;
    }


    @Override
    public String toString() {
        String correctTheme = null;
        int len = myTheme.length();
        if (len > 15) {
            correctTheme = myTheme.substring(0, 12) + "...";
        } else {
            correctTheme = myTheme;
        }
        return correctTheme + " " + myDate;
    }
}
