package pl.przemek;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * Created by Przemek on 2017-05-05.
 */
public class DiaryController implements Initializable{
    @FXML
    private TextField myTheme;
    @FXML
    private TextArea myDescription;
    @FXML
    private DatePicker myDatePicker;
    @FXML
    private ListView<LocalEvent> myListView;
    @FXML
    private Button addButton;
    @FXML
    private Button deleteButton;


    ObservableList<LocalEvent> list = FXCollections.observableArrayList();

    @FXML
    private void addEvent(Event e){
        if (check()) {
            list.add(new LocalEvent(LocalDate.now(), myTheme.getText(), myDescription.getText()));
            myListView.setItems(list);
            refresh();
        }
    }

    @FXML
    private void deleteEvent(){
        deleteFromList();
    }

    private void deleteFromList() {
        ObservableList<LocalEvent> item, removeItem;
        removeItem = myListView.getItems();
        item = myListView.getSelectionModel().getSelectedItems();
        item.forEach(removeItem::remove);
    }

    private boolean check(){
        boolean isEmpty = false;
        int themeLen = myTheme.getText().length();
        int descriptionLen = myDescription.getText().length();
        if (themeLen > 0 && descriptionLen >0){
            isEmpty = true;
        } else {
            AlertBox.alertBox("Information dialog", null, "Theme or description can't be empty!");
            isEmpty = false;
        }
        return isEmpty;
    }

    private void refresh(){
        myTheme.clear();
        myDescription.clear();
        myDatePicker.setValue(LocalDate.now());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        myDatePicker.setValue(LocalDate.now());
    }

}
